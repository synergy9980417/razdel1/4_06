import java.util.Random;
import java.util.Scanner;

public class Main {
public static void main(String[] args) throws Exception {

//    Урок 6.
//    Создание исключений, throw
//            1.
//    Доработайте крестики
//    -
//            нолики; создайте исключение, которое будете бросать
//    при неверном вводе пользователя
    System.out.println("!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!");
    System.out.println(
    "доработано и доступно по ссылке "+
    "https://gitlab.com/synergy9980417/tema3_urok10/"
    );


//    2.
//    Доработайте калькулятор: при неверном вводе выбрасывайте исключение

    System.out.println("!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!");

  //доработано здесь. razdel 1 tema 1 urok 5 punkt 7-й
    //https://gitlab.com/synergy9980417/tema1_urok1_5.git

    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!");
//    3.
//    Аналогичным образом, доработайте запрос курса валют на дату: при
//    некорректном вводе бросайте исключение. При отсутствии курса валют в
//    ответе, бросайте другое исключение .

    System.out.println("!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!");
    System.out.println(
    "исключение добавлено в 6-е подзадание по курсу валют по уроку 5 "+
"https://gitlab.com/synergy9980417/razdel1/tema4_urok5"
    );


    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!");
//    4.
//    Создайте 10 классов
//            -
//            исключений. Соберите их в массив. Пусть пользователь
//    выбирает, ка
//    кое по счету исключение выбросить.


 RuntimeException[] excArr4 = returnArr();

    System.out.println("Введите цифру, номер исключения");
    Scanner scanner=new Scanner(System.in);
    int i4=scanner.nextInt();
    if (i4<0||i4>excArr4.length){i4=0;}

  //      throw  excArr4[i4];



    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!");
//    5.
//    Соберите их в матрицу 3х3. Пусть пользователь выбирает номер столбца и
//    строки, какое выбросить исключение. Если ввод неверный
//            -
//            выбросите десятое.
    RuntimeException[][] excArr5 = new RuntimeException[3][3];
int k5=0;
    for (int i=0;i<3;i++){
    for (int j=0;j<3;j++) {
        excArr5[i][j] = excArr4[k5];
        k5++;
    }
}
    System.out.println("Введите строку и столбец, цифры от 0 до 2");
    int row=scanner.nextInt();
    int coll=scanner.nextInt();
    System.out.println("row = "+row+" coll="+coll);


//   if (row>=0&&row<3&&coll>=0&&coll<3)

    //System.out.println(excArr5[row][coll]);
//       throw excArr5[row][coll];
//   else {
//       throw excArr5[2][2];
//   }

//    6.
//    Сделайте функцию, которая возвращает случайное исключение из этих 10 (тип
//    возвращаемого значения будет общий: Exception). Выбросите это случайное
//            исключение

    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!");

    Random random = new Random();
    int k6 = random.nextInt(10);
    Exception ex6 = new Exception();
    ex6=excArr4[k6];
    throw ex6;

//    Критерии оценивания:
//    1 балл
//            -
//            создан новый проект в IDE
//    2 балла
//            -
//            написана общая структура программы
//    3 балла
//            -
//            написаны перечисленные программы без дополнительного
//    функциона
//    ла указанного в задании
//    4 балла
//            -
//            написаны перечисленные программы, со всем дополнительным
//    функционалом, допускается не более 5 критических замечаний в коде
//    5 баллов
//            -
//            все технические задания выполнены корректно, в полном объеме,
//    дополнительный функциона
//    л прописанный в пункте 3, 4, 5 выполнен корректно, в
//    полном объеме
//    Задание считается выполненным при условии, что слушатель получил оценку не
//    менее 3 баллов

}


    public static RuntimeException[] returnArr(){
        RuntimeException[] temp = new RuntimeException[10];
        temp[0]=new MyNewSystemException();
        temp[1]=new MyNewSystemException1();
        temp[2]=new MyNewSystemException2();
        temp[3]=new MyNewSystemException3();
        temp[4]=new MyNewSystemException4();
        temp[5]=new MyNewSystemException5();
        temp[6]=new MyNewSystemException6();
        temp[7]=new MyNewSystemException7();
        temp[8]=new MyNewSystemException8();
        temp[9]=new MyNewSystemException9();
        System.out.println("было добавлено "+temp.length+" исключений в массив MyNewSystemExceptionArray");
        return temp;
    }

}
